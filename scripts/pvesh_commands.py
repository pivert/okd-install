#!/usr/bin/env python
# cSpell:includeRegExp /".*",/
# cSpell:includeRegExp /\(f?".*"/
#
# Author      : François Delpierre
# Date        :
# Description : Print pvesh commands that can be copied & pasted on a Proxmox VE node to create VMs
#               This script reads commands from a YAML configuration file.
# License     : Gnu GPL
# Created on  : 2023-02-17

from __future__ import annotations

import argparse
from collections.abc import Sequence
import yaml


def main(argv: Sequence[str] | None = None) -> int:
    parser = argparse.ArgumentParser(description="A template doing nothing")
    # Add arguments here
    parser.add_argument(
        'action',
        choices=['create', 'stop', 'delete'],
        help='Action to show on VMs'
        )
    parser.add_argument(
        '--file', '-f',
        default='pvesh_config.yaml',
        help='YAML file defining your VMs'
    )

    args = parser.parse_args(argv)

    # Load YAML file
    with open(args.file, 'r') as f:
        data = yaml.safe_load(f)

    # Extract template, VM list, and config object
    template = data['template']
    vm_list = data['vm_list']
    config = data.get('config', {})
    first_vmid = config.get('vmid_from', 400)

    # Initialize vmid and pveid counters
    # We must substract 1, as it might be later added
    vmid = first_vmid - 1
    pveid = 0

    # Iterate over VM list
    for vm in vm_list:
        # Create dictionary for VM using template
        vm_dict = template.copy()

        # Override template with key-value pairs for VM
        vm_dict.update(vm)

        # Get vmid from vm dictionary
        # or use incremental number starting at first_vmid
        if 'vmid' in vm_dict:
            vmid = vm_dict['vmid']
        else:
            vmid += 1

        # Get pveid from vm dictionary
        # or use number between 1 and 3 and increment
        if 'pveid' in vm_dict:
            pveid = vm_dict['pveid']
        else:
            pveid = (pveid % 3) + 1

        if 'description' in vm_dict.keys():
            description = vm_dict['description']
        else:
            if 'desc_prepend' in config.keys():
                description = f"{config['desc_prepend']} {vm_dict['name'].title()}"
            else:
                description = vm_dict['name'].title()

        # Build pvesh command string
        if args.action == 'create':
            pvesh_cmd = (
                f"pvesh create /nodes/pve{pveid}/{vm_dict['type']}"
                f" -vmid {vmid} "
                f" -description '{description}'"
            )
            for key, value in vm_dict.items():
                if key not in ['vmid', 'pveid', 'type']:
                    pvesh_cmd += f" -{key} '{value}'"

            # Print pvesh command string for VM
            print(pvesh_cmd)

        else:
            print(f"pvesh create nodes/pve{pveid}/qemu/{vmid}/status/stop")
            if args.action == 'delete':
                print(f"pvesh delete nodes/pve{pveid}/qemu/{vmid}")

    return 0


if __name__ == "__main__":
    raise SystemExit(main())
