#!/usr/bin/env bash
# This script can be served with an http server and provided to each VM
# via the curl command below. But since your VM might not have network
# configured at boot, you might have to first enter the below
# commented lines by hand (from the console)
#
# sudo -i
# loadkeys be
# set -o vi
# nmcli con mod Wired\ connection\ 1 ipv4.addresses <hostIP>/24
# nmcli con mod Wired\ connection\ 1 ipv4.gateway 192.168.66.1
# nmcli con mod Wired\ connection\ 1 ipv4.method manual
# nmcli con up Wired\ connection\ 1 
# curl http://192.168.66.32:8081/net.sh | bash -s <HOSTNAME>

# Make sure you configure the next line to the web server you're
# running to serve the ignition files. 
# You can use `python3 -m http.server` to serve the current folder

WEB_SERVER_URL=http://192.168.232.31:8000

set -euo pipefail

echo "Setting hostname to $1"
nmcli general hostname $1
echo "Setting DNSs"
nmcli con mod Wired\ connection\ 1 ipv4.dns "192.168.10.31 192.168.10.32 192.168.10.33"
nmcli con mod Wired\ connection\ 1 ipv4.dns-search home.pivert.org
echo "Storing config"
nmcli con up Wired\ connection\ 1

echo "Installing OS in 5s"
sleep 5
# ${1%[012]} removes the trailing number from the hostname if any.
coreos-installer install -n -I $WEB_SERVER_URL/${1%[012]}.ign --insecure-ignition /dev/sda

echo "Rebooting in 60s"
sleep 60
reboot
